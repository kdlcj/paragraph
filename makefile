
NAME=a.out
CXX=g++
FLAGS=-g -std=c++17 -pthread
SAFE=-pedantic -Wall -Wextra -fsanitize=address
PROFILER=-pg
SRC=./src

main:
	$(CXX) $(SRC)/main.cpp -o $(NAME) $(FLAGS)

fast:
	$(CXX) $(SRC)/main.cpp -o $(NAME) $(FLAGS) -O4

profile:
	$(CXX) $(SRC)/main.cpp -o $(NAME) $(FLAGS) $(PROFILER)

clang:
	clang++ $(SRC)/main.cpp -o $(NAME) $(FLAGS) $(SAFE) -latomic

safe:
	$(CXX) $(SRC)/main.cpp -o $(NAME) $(FLAGS) $(SAFE) 

clean:
	rm *.tra *.lab *.explicit *.out *.png *.dot *.pm *.pctl; clear; ls
