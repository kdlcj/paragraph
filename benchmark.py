#!/usr/bin/python3

import time
import os
import matplotlib.pyplot as pp
import numpy as np

##############################
graphSize      = 10
graphDesc      = "random"
maxThreadCount = 1
repetitions    = 1
##############################

imageFolder    = "misc"
hostName       = os.popen("uname -n").read()
commandsToRun  = [
                    ("storm, single thread: input+comp", lambda threadCount, graphSize: os.system("~/Documents/storm/build/bin/storm --explicit explicit.tra explicit.lab --prop \"P =? [F \\\"terminal\\\"]\"")),\
                    ("paragraph: generation+comp",       lambda threadCount, graphSize: os.system("./a.out" + " " + str(threadCount + 1) + " " + str(graphSize) + " 0 " + graphDesc)),\
                 ]

def timeToThreads():
    results = []
    for i, commandToRun in enumerate(commandsToRun):
        print("measuring", commandsToRun[i][0])
        commandResults = []
        for threadCount in range(maxThreadCount):
            print(threadCount + 1, "/", maxThreadCount)
            for n in range(repetitions):
                start = time.time()
                commandToRun[1](threadCount, graphSize)
                end = time.time()
                if (len(commandResults) - 1 < threadCount):
                    commandResults.append((threadCount + 1, end - start))
                else:
                    commandResults[threadCount] = (threadCount + 1, (commandResults[threadCount][1] + (end - start)) / 2)
        results.append(commandResults)
    print("measuring finished")
    return results

def info():
    print("graph size:", graphSize) 
    print("graph description:", graphDesc) 
    print("max thread count:", maxThreadCount)
    print("repetitions:", repetitions)

def plot(totalResults):
    maxY = 0
    maxX = 0
    for i in range(len(totalResults)):
        x = [a[0] for a in totalResults[i]]
        y = [a[1] for a in totalResults[i]]
        maxY = max(y) if max(y) > maxY else maxY
        maxX = max(x) if max(x) > maxX else maxX
        pp.plot(x, y, '^')
        pp.plot(x, y, label=commandsToRun[i][0])
    pp.yticks(np.arange(0, maxY * 1.1, (maxY / 20)))
    pp.xticks(np.arange(1, maxThreadCount * 1.1, max([1, (maxX / 20)]) ))
    pp.ylabel("time in seconds")
    pp.xlabel("number of threads")
    pp.title(graphDesc + " graph of size " + str(graphSize) + ", time is average of " + str(repetitions) + " measurements on " + hostName)
    pp.legend()
    pp.savefig(fname = imageFolder + "/" + graphDesc + "_" + str(graphSize) + "_" + str(time.time())[:-8] + ".png", bbox_inches='tight', dpi = 500)
    pp.show()


info()
os.system("./a.out 2 " + str(graphSize) + " 1 " + graphDesc)
plot(timeToThreads())

