
#ifndef NODE_HPP
#define NODE_HPP

#include <algorithm>
#include <atomic>
#include <chrono>
#include <fstream>
#include <iostream>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <vector>

#include "Expression.hpp"

class Node {
public:
    struct Edge {
        Edge(Node* n, Expression* e): target(n), weight(e) {}
        Edge() = default;
        Node*       target;
        Expression* weight;
    };

    enum Node_State_Enum {
        FREE, OCCUPIED, LOCKED, BEING_MODIFIED, DELETED
    };

    struct Node_State {
        Node_State_Enum name;
        int modifier_count;
        bool operator==(Node_State rhs) {
            return name == rhs.name && modifier_count == rhs.modifier_count;
        }
        bool operator!=(Node_State rhs) {
            return !(*this == rhs);
        }
    };

    Node() {
        state_ = {FREE, 0};
        visited<0>(false);
        visited<1>(false);
    }

    Node(const Node& rhs) {
        state_    = {FREE, 0};
        ingoing_  = rhs.ingoing_;
        outgoing_ = rhs.outgoing_;
        visited<0>(false);
        visited<1>(false);
    }

    Node(Node&& rhs) {
        state_    = {FREE, 0};
        ingoing_  = std::move(rhs.ingoing_);
        outgoing_ = std::move(rhs.outgoing_); 
        visited<0>(false);
        visited<1>(false);
    }

    Node& operator=(const Node& rhs) {
        state_    = {FREE, 0};
        ingoing_  = rhs.ingoing_;
        outgoing_ = rhs.outgoing_; 
        visited<1>(false);
        visited<0>(false);
        return *this;
    }

    ~Node() = default;

    bool delete_node() {
        if (state_.load().name == DELETED) {
            return true; 
        }

        if (!lock_node()) {
            return false;
        }

        normalize_edges();
        mark_neighbourhood();
        switch_pointers();
        unmark_neighbourhood();

        Node_State node_state = {LOCKED, 0};
        auto r = state_.compare_exchange_strong(node_state, {DELETED, 0});

        return r;
    }

    Node& make_edge(Node* another_node, std::string edge_name) {
        make_edge(another_node, new Primitive(edge_name));
        return *this;
    }

    Node& make_edge(Node* another_node, double value) {
        make_edge(another_node, new Numeric(value));
        return *this;
    }

    Node& make_edge(Node* another_node, Expression* expression = new Primitive("a")) {
        expression_owner_.push_back(std::unique_ptr<Expression>(expression));
        outgoing_.push_back({another_node, expression});
        another_node->ingoing_.insert(this);
        return *this;
    }

    Node& normalize_edges() {
        std::sort(outgoing_.begin(), outgoing_.end(), [](auto& a, auto& b){ return a.target > b.target; });
        remove_duplicates();
        remove_self_loop();
        return *this;
    }

    Node_State state() const {
        return state_.load();
    }

    std::mutex& outgoing_lock() {
        return outgoing_lock_;
    }

    std::mutex& ingoing_lock() {
        return ingoing_lock_;
    }

    const std::vector<Edge>& outgoing() const {
        return outgoing_;
    }

    const std::set<Node*>& ingoing() const {
        return ingoing_;
    }

    template<int index>
    bool visited() const {
        return std::get<index>(visited_).load();
    }

    template<int index>
    void visited(bool b) {
        std::get<index>(visited_).store(b);
    }

    bool complete_visited() {
        return visited<0>() && visited<1>();
    }

private:
    bool lock_node() {
        Node_State node_state;
        if (!state_.compare_exchange_strong(node_state = {FREE, 0}, {OCCUPIED, 0})) {
            return false;
        }
        {
            std::unique_lock<std::mutex> lock(outgoing_lock_, std::try_to_lock);
            if (!lock.owns_lock()) {
                return false;
            }
            for (const auto& edge: outgoing_) {
                if (edge.target != this && !check_neighbour(edge.target)) {
                    return false;
                }
            }
        }
        {
            std::unique_lock<std::mutex> lock(ingoing_lock_, std::try_to_lock);
            if (!lock.owns_lock()) {
                return false;
            }
            for (Node* node: ingoing_) {
                if (node != this && !check_neighbour(node)) {
                    return false;
                }
            }
        }
        return state_.compare_exchange_strong(node_state = {OCCUPIED, 0}, {LOCKED, 0});
    }

    bool check_neighbour(Node* node) {
        Node_State node_state = node->state_.load();
        if (node_state.name == LOCKED) {
            state_.compare_exchange_strong(node_state = {OCCUPIED, 0}, {FREE, 0});
            return false;
        }
        if (node_state.name == OCCUPIED) {
            if (node > this) {
            	state_.compare_exchange_strong(node_state = {OCCUPIED, 0}, {FREE, 0});
                return false;
            }
            if (!node->state_.compare_exchange_strong(node_state = {OCCUPIED, 0}, {FREE, 0})) {
                if (node_state.name != FREE || node_state.modifier_count != 0) {
            		state_.compare_exchange_strong(node_state, {FREE, 0});
                    return false;
                }
            }
        }
        return true;
    }

    void mark_neighbourhood() {
        Node_State node_state;
        bool succ;

        for (auto& edge: outgoing_) {
            succ = false;
            node_state = edge.target->state_.load();
            while (!succ) {
                if (edge.target == this) {
                    succ = true;
                } else if (node_state.modifier_count == 0) {
                    succ = edge.target->state_.compare_exchange_weak(node_state = {FREE, 0}, {BEING_MODIFIED, 1});
                    succ = succ || edge.target->state_.compare_exchange_weak(node_state = {OCCUPIED, 0}, {BEING_MODIFIED, 1});
                } else {
                    succ = edge.target->state_.compare_exchange_weak(node_state, {BEING_MODIFIED, node_state.modifier_count + 1});
                }
            }
        }

        for (auto& node: ingoing_) {
            succ = false;
            node_state = node->state_.load();
            while (!succ) {
                if (node == this) {
                    succ = true;
                } else if (node_state.modifier_count == 0) {
                    succ = node->state_.compare_exchange_weak(node_state = {FREE, 0}, {BEING_MODIFIED, 1});
                    succ = succ || node->state_.compare_exchange_weak(node_state = {OCCUPIED, 0}, {BEING_MODIFIED, 1});
                } else {
                    succ = node->state_.compare_exchange_weak(node_state, {BEING_MODIFIED, node_state.modifier_count + 1});
                }
            }
        }
    }

    void unmark_neighbourhood() {
        bool succ;
        Node_State node_state;
        for (auto& edge: outgoing_) {
            succ = false;
            node_state = edge.target->state_.load();
            while (!succ) {
                if (edge.target == this) {
                    succ = true;
                } else if (node_state.modifier_count == 1) {
                    succ = edge.target->state_.compare_exchange_weak(node_state, {FREE, 0});
                } else {
                    succ = edge.target->state_.compare_exchange_weak(node_state, {BEING_MODIFIED, node_state.modifier_count - 1});
                }
            }
        }
        for (auto& node: ingoing_) {
            succ = false;
            node_state = node->state_.load();
            while (!succ) {
                if (node == this) {
                    succ = true;
                } else if (node_state.modifier_count == 1) {
                    succ = node->state_.compare_exchange_weak(node_state, {FREE, 0});
                } else {
                    succ = node->state_.compare_exchange_weak(node_state, {BEING_MODIFIED, node_state.modifier_count - 1});
                }
            }
        }
    }

    void remove_duplicates() {
        Edge* last_unique = &outgoing_.front();

        for (std::size_t i = 0; i < outgoing_.size(); i++) {
            if (outgoing_[i].target == last_unique->target && outgoing_[i].weight != last_unique->weight) {
                expression_owner_.push_back(std::make_unique<Plus>(last_unique->weight, outgoing_[i].weight));
                last_unique->weight = expression_owner_.back().get();
                outgoing_[i].target = nullptr;
            } else if (outgoing_[i].target != last_unique->target) {
                last_unique = &outgoing_[i]; 
            }
        }

        outgoing_.erase(std::remove_if(outgoing_.begin(),
                                       outgoing_.end(),
                                       [](auto& e){ return e.target == nullptr; }),
                        outgoing_.end());
    }

    void remove_self_loop() {
        Edge looped_edge;
        bool exists = false;
        for (auto& edge: outgoing_) {
            if (edge.target == this) {
                exists = true;
                looped_edge = edge;
                std::swap(edge, outgoing_.back());
                outgoing_.resize(outgoing_.size() - 1);
                break;
            }
        }

        if (exists) {
            for (auto& edge: outgoing_) {
                expression_owner_.push_back(std::make_unique<Star>(looped_edge.weight));
                expression_owner_.push_back(std::make_unique<Times>(expression_owner_.back().get(), edge.weight));
                edge.weight = expression_owner_.back().get();
            }
        }
    }

    void switch_pointers() {
        make_new_links();
        remove_old_links();
    }

    void remove_old_links() {
        for (auto& edge: outgoing_) {
            std::lock_guard<std::mutex> lock(edge.target->ingoing_lock_);
            edge.target->ingoing_.erase(this);
        }
        for (Node* node: ingoing_) {
            std::lock_guard<std::mutex> lock(node->outgoing_lock_);
            for (std::size_t i = 0; i < node->outgoing_.size(); i++) {
                if (node->outgoing_[i].target == this) {
                    node->outgoing_[i].target = nullptr;
                }
            }
            node->outgoing_.erase(std::remove_if(node->outgoing_.begin(),
                                                 node->outgoing_.end(),
                                                 [](auto& e){ return e.target == nullptr; }),
                                  node->outgoing_.end());
        }
    }

    void make_new_links() {
        std::vector<Edge> temporary_edges;
        for (Node* node: ingoing_) {
            for (auto& edge: outgoing_) {
                {
                    std::lock_guard<std::mutex> lock(node->outgoing_lock_);
                    for (auto& edge_from_ingoing: node->outgoing_) {
                        if (edge_from_ingoing.target == this) {
                            expression_owner_.push_back(std::make_unique<Times>(edge_from_ingoing.weight, edge.weight));
                            temporary_edges.push_back(Edge(edge.target, expression_owner_.back().get()));
                        } 
                    }
                    for (auto& temporary_edge: temporary_edges) {
                        node->outgoing_.push_back(temporary_edge); 
                    }
                    temporary_edges.clear();
                }
                {
                    std::lock_guard<std::mutex> lock(edge.target->ingoing_lock_);
                    edge.target->ingoing_.insert(node);
                }
            }
        }
    }

    std::atomic<Node_State> state_;
    std::vector<Edge>       outgoing_;
    std::set<Node*>         ingoing_;
    std::mutex              outgoing_lock_;
    std::mutex              ingoing_lock_;
    std::vector<std::unique_ptr<Expression>> expression_owner_;
    std::pair<std::atomic<bool>, std::atomic<bool>> visited_;
};

#endif
