
#ifndef PARAGRAPH
#define PARAGRAPH

#include <algorithm>
#include <cctype>
#include <cstdint>
#include <deque>
#include <iostream>
#include <limits>
#include <sstream>
#include <stack>
#include <thread>
#include <tuple>

#include "Node.hpp"


class Paragraph {
public: 
    friend std::istream& operator>>(std::istream&, Paragraph&);

    Paragraph(std::size_t thread_count = 4)
    : number_of_threads_(thread_count)
    , work_assignment_(thread_count)
    {}

    ~Paragraph() = default;

    void run() {
        std::vector<std::thread> threads;


        /* bi-directional broadcast reachability check */
        std::thread downer(broadcast<false>, &initial(), this);
        std::thread upper(broadcast<true>, &terminal(), this);

        /* node-removing workers */
        for (std::size_t i = 0; i < number_of_threads_; i++) {
            threads.push_back(std::thread(thread_function, this, i));
        }

        /* after traversal, unreachable nodes are not removed */
        downer.join();
        upper.join();
        traverse_finish_ = true;

        for (auto& thread: threads) {
            thread.join();
        }
    }

    const std::vector<Node>& graph() const {
        return graph_;
    }

    std::vector<Node>& graph() {
        return graph_;
    }

    Node& initial() {
        return initial_; 
    }
    
    const Node& initial() const {
        return initial_; 
    }

    Node& terminal() {
        return terminal_; 
    }

    const Node& terminal() const {
        return terminal_; 
    }

    Expression* result() {
        initial_.normalize_edges();
        for (const auto& final_edge: initial_.outgoing()) {
            if (final_edge.target == &terminal_) {
                return final_edge.weight;
            } 
        } 
        return nullptr;
    }

private:
    template<bool up>
    static void broadcast(Node* start, Paragraph* p) {
        std::stack<Node*> stack({start});
        std::vector<std::thread> threads;
        while (!stack.empty()) {
            Node* current_node = stack.top(); 
            stack.pop();
            if (current_node->visited<up? 0: 1>()) {
                return; 
            }
            current_node->visited<up? 0: 1>(true);
                
            if (up) {
                std::lock_guard<std::mutex> l(current_node->ingoing_lock());
                for (auto& node: current_node->ingoing()) {
                    if (!node->visited<up? 0: 1>()) {
                        stack.push(node);    
                    }
                }
            } else {
                std::lock_guard<std::mutex> l(current_node->outgoing_lock());
                for (auto& edge: current_node->outgoing()) {
                    if (!edge.target->visited<up? 0: 1>()) {
                        stack.push(edge.target);    
                    }
                }
            }
        }
    }

    void generate_work_assignment(std::size_t thread_index) {
        std::size_t start = thread_index * (graph_.size() / number_of_threads_);
        std::size_t end;

        if (graph_.empty()) {
            return;
        }

        if (thread_index + 1 == number_of_threads_) {
            end = graph_.size() - 1;
        } else {
            end = (thread_index + 1) * (graph_.size() / number_of_threads_);
            end = end == 0? 0: end - 1;
        }

        for (std::size_t i = start; i <= end; i++) {
            work_assignment_[thread_index].push_back(&graph_[i]);
        }
    }

    void static thread_function(Paragraph* paragraph, std::size_t thread_index) {
        paragraph->generate_work_assignment(thread_index);

        while (!paragraph->work_assignment_[thread_index].empty()) {
            Node* current_node = paragraph->work_assignment_[thread_index].front();
            paragraph->work_assignment_[thread_index].pop_front();
            
            if (paragraph->traverse_finish_ && !current_node->complete_visited()) {
                continue; 
            }
            if (!current_node->delete_node()) {
                paragraph->work_assignment_[thread_index].push_back(current_node); 
            }
        }
    }

    const std::size_t              number_of_threads_;
    std::vector<std::deque<Node*>> work_assignment_;
    std::vector<Node>              graph_;
    Node                           initial_, terminal_;
    std::atomic<bool>              traverse_finish_{false};
};

std::istream& operator>>(std::istream& stream, Paragraph& paragraph)
{
    std::string line;
    std::size_t from, to;
    std::string edge_expression;
    std::vector<std::tuple<std::size_t, std::size_t, std::string>> future_nodes;
	std::size_t max = 0;
    std::getline(stream, line);
    while (std::getline(stream, line)) {
        std::stringstream string_stream(line);
        string_stream >> from;
        string_stream >> to;
        string_stream >> edge_expression;
        max = max < from? from: max;
        max = max <   to?   to: max;
        future_nodes.push_back(std::make_tuple(from, to, edge_expression));
    }

    paragraph.graph_.clear();
    paragraph.graph_.resize(max + 1);

    for (const auto& triplet: future_nodes) {
        from            = std::get<0>(triplet);
        to              = std::get<1>(triplet);
        edge_expression = std::get<2>(triplet);
        paragraph.graph_.at(from).make_edge(&paragraph.graph_[to], edge_expression);
    }
    return stream; 
}

#endif
