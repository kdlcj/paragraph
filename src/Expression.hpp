
#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <iostream>
#include <string>
#include <memory>

class Expression {
public:
    virtual std::string print() const         = 0;
    virtual std::string to_dot() const        = 0;
    virtual std::string dot_node_name() const = 0;
    virtual ~Expression()                     = default;
    virtual double evaluate() const           = 0;

    const bool as_rational_                   = true;
};



class Star: public Expression {
public:
    Star(Expression* ex): ex_(ex) {}

    std::string print() const override {
        if (as_rational_) {
            return "(1/(1 - " + ex_->print() + "))";
        }
        return "(" + ex_->print() + ")*";
    }

    double evaluate() const override {
        return (1/(1 - ex_->evaluate()));
    }

    std::string dot_node_name() const override {
        return "\"" + std::to_string((long) this) + " STAR\"";
    }

    std::string to_dot() const override {
        return "\t" + dot_node_name() + " -> " + ex_->dot_node_name() + "\n" + ex_->to_dot() + "\n";
    }

private:
    const Expression* ex_; 
};



class Times: public Expression {
public:
    Times(Expression* ex1, Expression* ex2)
    : ex1_(ex1)
    , ex2_(ex2) {}

    std::string print() const override {
        if (as_rational_) {
            return "(" + ex1_->print() + " * " + ex2_->print() + ")";
        }
        return "(" + ex1_->print() + " · " + ex2_->print() + ")";
    }

    double evaluate() const override {
        return ex1_->evaluate() * ex2_->evaluate();
    }

    std::string to_dot() const override {
        return "\t" + dot_node_name() + " -> " + ex1_->dot_node_name() + "\n" + 
               "\t" + dot_node_name() + " -> " + ex2_->dot_node_name() + "\n" +
               ex1_->to_dot() + "\n" + ex2_->to_dot() + "\n";
    }

    std::string dot_node_name() const override {
        return "\"" + std::to_string((long) this) + " TIMES\"";
    }

private:
    const Expression* ex1_;
    const Expression* ex2_;
};



class Plus: public Expression {
public:
    Plus(Expression* ex1, Expression* ex2)
    : ex1_(ex1)
    , ex2_(ex2) {}

    std::string print() const override {
        return "(" + ex1_->print() + " + " + ex2_->print() + ")";
    }

    std::string to_dot() const override {
        return "\t" + dot_node_name() + " -> " + ex1_->dot_node_name() + "\n" + 
               "\t" + dot_node_name() + " -> " + ex2_->dot_node_name() + "\n" +
               ex1_->to_dot() + "\n" + ex2_->to_dot() + "\n";
    }

    double evaluate() const override {
        return ex1_->evaluate() + ex2_->evaluate();
    }

    std::string dot_node_name() const override {
        return "\"" + std::to_string((long) this) + " PLUS\"";
    }

private:
    const Expression* ex1_;
    const Expression* ex2_;
};



class Primitive: public Expression {
public:
    Primitive(std::string name): name_(name) {}

    std::string print() const override {
        return name_;
    }

    std::string to_dot() const override {
        return "\t" + dot_node_name();
    }

    double evaluate() const override {
        return 0;
    }

    std::string dot_node_name() const override {
        return "\"" + std::to_string((long) this) + " PRIMITIVE" + name_ + "\"";
    }

private:
    const std::string name_;
};



class Numeric: public Expression {
public:
    Numeric(double number): value_(number) {}

    std::string print() const override {
        return std::to_string(value_);
    }

    std::string to_dot() const override {
        return "\t" + dot_node_name();
    }

    double evaluate() const override {
        return value_;
    }

    std::string dot_node_name() const override {
        return "\"" + std::to_string((long) this) + " NUMERIC" + std::to_string(value_) + "\"";
    }
private:
    const double value_;
};

#endif
