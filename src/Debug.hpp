
#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <cstdlib>
#include <ctime>
#include <functional>

class Debug {
public:
    struct graphs {
        static void tree(Paragraph& p, std::size_t size) {
            std::cout << "generating tree... " << std::flush;
            std::function<std::size_t(std::size_t)> s = [&](std::size_t i) -> std::size_t {
                if (i == 0) { return 0; } 
                if (i == 1) { return 3; } 
                return 2 * s(i - 1) + 1;
            };
            auto jump = [](std::size_t from) {
                return from * 2 + 1; 
            };
            p.graph().clear();
            p.graph().resize(s(size));
            for (std::size_t i = 0; i < p.graph().size(); i++) {
                auto to = jump(i);
                if (to < p.graph().size()) {
                    p.graph()[i].make_edge(&p.graph()[to], 0.5);
                    p.graph()[i].make_edge(&p.graph()[to + 1], 0.5);
                }
            }

            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }

        static void random(Paragraph& p, std::size_t size, double density = 1, unsigned seed = 1) {
            std::cout << "generating random... " << std::flush;
            p.graph().clear();
            p.graph().resize(size);
            std::srand(seed);
            for (auto& node: p.graph()) {
                std::size_t outgoing_edges = 1 + (std::rand() % (size - 1)) * density;
                for (std::size_t i = 0; i < outgoing_edges; i++) {
                    std::size_t x = std::rand() % size;
                    node.make_edge(&p.graph()[x], 1. / outgoing_edges);  
                    node.normalize_edges();
                }
            }

            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }

        static void from_console(Paragraph& p) {
            std::cout << "loading from console... " << std::flush;
            p.graph().clear();
            std::cin >> p; 

            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }

        static void from_file(Paragraph& p, std::string filename) {
            std::cout << "loading from file... " << std::flush;
            std::ifstream file(filename);
            p.graph().clear();
            file >> p;
            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }

        static void line(Paragraph& p, std::size_t length) {
            std::cout << "generating line... " << std::flush;
            p.graph().clear();
            p.graph().resize(length);
            for (std::size_t i = 0; i < p.graph().size() - 1; i++) {
                p.graph()[i].make_edge(&p.graph()[i + 1], 1);
            }
            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }

        static void complete(Paragraph& p, std::size_t size) {
            std::cout << "generating complete graph... " << std::flush;
            p.graph().clear();
            p.graph().resize(size);
            for (auto a = 0u; a < size; a++) {
                for (auto b = 0u; b < size; b++) {
                    p.graph().at(a).make_edge(&p.graph().at(b), 1./size);
                }
            }
            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }

        static void empty(Paragraph& p, std::size_t size) {
            std::cout << "generating empty graph... " << std::flush;
            p.graph().clear();
            p.graph().resize(size);
            p.initial().make_edge(&p.graph().front(), 1);
            p.graph().back().make_edge(&p.terminal(), 1);
            std::cout << "done." << std::endl;
        }
    };

    static void generate_dot_file(const Paragraph& p, std::string filename = "out.dot") {
        generate_dot_file(p.graph(), filename);
    }


    static void generate_prism_file(const Paragraph& p, std::string filename = "prism.pm") {
        std::string start, params;
        std::ofstream file;
        std::ofstream pctl_file;
        bool first;
        auto quattro = [](const Node* x, int pos) {
            unsigned long int i = (unsigned long int) x;
            std::uint16_t* c = (std::uint16_t*) &i; 
            return std::to_string(c[pos]);
        };
        auto is_number = [](std::string string) {
            for (auto c: string) {
                if (!std::isdigit(c) && c != '.') {
                    return false;
                }
            }
            return true;
        };
        file.open(filename);
        for (const auto& node: p.graph()) {
            if (node.outgoing().empty()) {
                continue; 
            }
            start += "\t[] (s=" + quattro(&node, 0) + ") & (t=" + quattro(&node, 1) + ")" +
                     " & (u=" + quattro(&node, 2) + ") & (v=" + quattro(&node, 3) + ")";
            first = true;
            for (const auto& edge: node.outgoing()) {
                start += (first? " -> ": " + ") +
                    edge.weight->print() + " : " +
                    "(s'=" + quattro(edge.target, 0) + ") & (t'=" +
                    quattro(edge.target, 1) + ") & (u'=" + quattro(edge.target, 2) +
                    ") & (v'=" + quattro(edge.target, 3) + ")";
                if (!is_number(edge.weight->print())) {
                    params += "const double " + edge.weight->print() + ";\n";
                }
                first = false;
            }
            start += ";\n";
        }
        pctl_file.open(filename + ".pctl");
        pctl_file << "P=?[ F (s=" + quattro(&p.graph().back(), 0) + ")" +
                          "& (t=" + quattro(&p.graph().back(), 1) + ") " +
                          "& (u=" + quattro(&p.graph().back(), 2) + ") " +
                          "& (v=" + quattro(&p.graph().back(), 3) + ") ]";
        file << "// --prop \"P=?[ F (s=" + quattro(&p.graph().back(), 0) +
                               ") & (t=" + quattro(&p.graph().back(), 1) + ") " +
                                 "& (u=" + quattro(&p.graph().back(), 2) + ") " +
                                 "& (v=" + quattro(&p.graph().back(), 3) + ") ]\"\ndtmc\n\n" + params +
                "\nmodule paragraph\n" +
                   "\ts : [0..65535] init " + quattro(&p.graph().front(), 0) +
                ";\n\tt : [0..65535] init " + quattro(&p.graph().front(), 1) +
                ";\n\tu : [0..65535] init " + quattro(&p.graph().front(), 2) +
                ";\n\tv : [0..65535] init " + quattro(&p.graph().front(), 3) +
                ";\n\n" + start + "endmodule\n";
    }   

    static void generate_dot_file(const std::vector<Node>& node_array, std::string filename = "out.dot") {
        std::cout << "generating dot file... " << std::flush;
        std::string start = std::string("digraph D {\n") +
                            "layout = \"circo\"" +
                            "graph [fontname = \"monaco\"];\n" +
                            "node [fontname = \"monaco\" fontsize=\"10\" shape=\"box\"];\n" +
                            "edge [fontname = \"monaco\" fontsize=\"5\"];\n";
        std::string end   = "}\n";
        std::string all = start;
        std::ofstream file(filename);
        for (std::size_t i = 0; i < node_array.size(); i++) {
            all += "\t\"" + std::to_string((std::size_t) (&node_array[i]))
                + " " + state_to_string(node_array[i].state().name)
                + std::to_string(node_array[i].state().modifier_count) + "\"\n";
            for (std::size_t j = 0; j < node_array[i].outgoing().size(); j++) {
                if (node_array[i].state().name == Node::Node_State_Enum::DELETED) {
                    break;
                }
                all += "\t\"" + std::to_string((std::size_t) (&node_array.at(i))) + " "
                    + state_to_string(node_array.at(i).state().name)
                    + std::to_string(node_array.at(i).state().modifier_count)
                    + "\" -> \""
                    + std::to_string((std::size_t) node_array.at(i).outgoing().at(j).target)
                    + " " + state_to_string(node_array.at(i).outgoing().at(j).target->state().name)
                    + std::to_string(node_array[i].outgoing()[j].target->state().modifier_count)
                    + "\"[label=\" " + node_array[i].outgoing()[j].weight->print() + "\"]\n";
            }
        }
        file << all + end << std::flush;
        std::cout << "done." << std::endl;
    }

    static void generate_explicit_file(const Paragraph& p, std::string filename = "explicit") {
        std::cout << "generating explicit file... " << std::flush;
        std::ofstream transitions(filename + ".tra");
        std::ofstream labels(filename + ".lab");
        transitions << "dtmc\n"; 
        for (std::size_t i = 0; i < p.graph().size(); i++) {
            if (p.graph()[i].outgoing().empty() || p.graph()[i].outgoing().front().target == &p.terminal()) {
                transitions << i << " " << i << " 1\n"; 
            }
            for (const auto& edge: p.graph()[i].outgoing()) {
                if (edge.target != &p.terminal()) {
                    transitions << i << " " << edge.target - &p.graph()[0] << " " << edge.weight->evaluate() << "\n";
                }
            } 
        }
        labels << "#DECLARATION\ninit terminal\n#END\n0 init\n" << p.graph().size() - 1 << " terminal\n";
        std::cout << "done." << std::endl;
    }
    
    static void generate_dot_file_expression_tree(Expression* weight, std::string filename = "expression.dot") {
        std::cout << "generating dot file of expression tree... " << std::flush;
        std::string start = "strict digraph D {\n",
                      end = "}";
        std::ofstream file(filename);
        file << start + weight->to_dot() << end;
        std::cout << "done." << std::endl;
    }
    
    static std::string state_to_string(Node::Node_State_Enum state) {
        switch (state) {
        case Node::Node_State_Enum::FREE:
            return "free";
        case Node::Node_State_Enum::OCCUPIED:
            return "occupied";
        case Node::Node_State_Enum::BEING_MODIFIED:
            return "being modified";
        case Node::Node_State_Enum::LOCKED:
            return "locked";
        case Node::Node_State_Enum::DELETED:
            return "deleted";
        default:
            return "";
        }
    }
};

#endif
