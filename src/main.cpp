
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <iostream>

#include "Paragraph.hpp"
#include "Debug.hpp"

int main(int arg_count, char* arg_vector[]) {
    std::size_t number_of_threads = 4;
    std::size_t graph_size        = 18;
    std::string graph_type        = "random";
    bool generate_export          = false;
    
    if (arg_count > 1) {
        number_of_threads = std::atoi(arg_vector[1]);
    }
    if (arg_count > 2) {
        graph_size = std::atoi(arg_vector[2]); 
    }
    if (arg_count > 3) {
        generate_export = std::atoi(arg_vector[3]) != 0;
    }
    if (arg_count > 4) {
        graph_type = arg_vector[4]; 
    }
 
    Paragraph p(number_of_threads);
 
    auto dstart = std::chrono::steady_clock::now();
    for (auto& c: graph_type) c = std::tolower(c);

    if (graph_type == "random") {
        Debug::graphs::random(p, graph_size, 0.3);
    }
    else if (graph_type == "line") {
        Debug::graphs::line(p, graph_size);
    }
    else if (graph_type == "empty") {
        Debug::graphs::empty(p, graph_size);
    }
    else if (graph_type == "complete") {
        Debug::graphs::complete(p, graph_size);
    }
    else if (graph_type == "tree") {
        Debug::graphs::tree(p, graph_size);
    }
    auto dend = std::chrono::steady_clock::now();

    if (generate_export) {
        Debug::generate_explicit_file(p);
        Debug::generate_prism_file(p);
        return 0;
    }

    std::cout << "Graph size: " << p.graph().size() << " nodes, which is " << p.graph().size() * sizeof(Node) << " bytes."  << std::endl;

    auto start = std::chrono::steady_clock::now();
    p.run();
    auto end = std::chrono::steady_clock::now();

    std::cout << "Time for data generation: " << std::chrono::duration_cast<std::chrono::milliseconds>(dend - dstart).count() << " milliseconds.\n";
    std::cout << "Time for model checking: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " milliseconds.\n";

    //Debug::generate_dot_file({p.initial(), p.terminal()}, "after.dot");
    //std::cout << "  resulting expression is: \n" << p.result()->print() << "\n";
    //std::cout << "  also enumerated in double precision as " << p.result()->evaluate() << "\n";

    //Debug::generate_dot_file_expression_tree(p.result());
}
